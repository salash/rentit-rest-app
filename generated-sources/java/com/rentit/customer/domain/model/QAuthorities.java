package com.rentit.customer.domain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QAuthorities is a Querydsl query type for Authorities
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAuthorities extends EntityPathBase<Authorities> {

    private static final long serialVersionUID = -798301787L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAuthorities authorities = new QAuthorities("authorities");

    public final StringPath authority = createString("authority");

    public final QAuthorityID id;

    public final StringPath username = createString("username");

    public QAuthorities(String variable) {
        this(Authorities.class, forVariable(variable), INITS);
    }

    public QAuthorities(Path<? extends Authorities> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QAuthorities(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QAuthorities(PathMetadata<?> metadata, PathInits inits) {
        this(Authorities.class, metadata, inits);
    }

    public QAuthorities(Class<? extends Authorities> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QAuthorityID(forProperty("id")) : null;
    }

}

