package com.rentit.customer.domain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QAuthorityID is a Querydsl query type for AuthorityID
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QAuthorityID extends BeanPath<AuthorityID> {

    private static final long serialVersionUID = -798287326L;

    public static final QAuthorityID authorityID = new QAuthorityID("authorityID");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QAuthorityID(String variable) {
        super(AuthorityID.class, forVariable(variable));
    }

    public QAuthorityID(Path<? extends AuthorityID> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAuthorityID(PathMetadata<?> metadata) {
        super(AuthorityID.class, metadata);
    }

}

