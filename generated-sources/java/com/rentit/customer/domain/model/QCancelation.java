package com.rentit.customer.domain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QCancelation is a Querydsl query type for Cancelation
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCancelation extends EntityPathBase<Cancelation> {

    private static final long serialVersionUID = 1204475711L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCancelation cancelation = new QCancelation("cancelation");

    public final QCustomerID customerId;

    public final QCancelationID id;

    public final com.rentit.sales.domain.model.QPurchaseOrderID poID;

    public QCancelation(String variable) {
        this(Cancelation.class, forVariable(variable), INITS);
    }

    public QCancelation(Path<? extends Cancelation> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCancelation(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCancelation(PathMetadata<?> metadata, PathInits inits) {
        this(Cancelation.class, metadata, inits);
    }

    public QCancelation(Class<? extends Cancelation> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.customerId = inits.isInitialized("customerId") ? new QCustomerID(forProperty("customerId")) : null;
        this.id = inits.isInitialized("id") ? new QCancelationID(forProperty("id")) : null;
        this.poID = inits.isInitialized("poID") ? new com.rentit.sales.domain.model.QPurchaseOrderID(forProperty("poID")) : null;
    }

}

