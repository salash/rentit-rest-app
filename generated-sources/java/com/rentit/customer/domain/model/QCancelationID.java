package com.rentit.customer.domain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QCancelationID is a Querydsl query type for CancelationID
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QCancelationID extends BeanPath<CancelationID> {

    private static final long serialVersionUID = -2140009318L;

    public static final QCancelationID cancelationID = new QCancelationID("cancelationID");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QCancelationID(String variable) {
        super(CancelationID.class, forVariable(variable));
    }

    public QCancelationID(Path<? extends CancelationID> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCancelationID(PathMetadata<?> metadata) {
        super(CancelationID.class, metadata);
    }

}

