package com.rentit.customer.domain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QCustomerID is a Querydsl query type for CustomerID
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QCustomerID extends BeanPath<CustomerID> {

    private static final long serialVersionUID = 170875509L;

    public static final QCustomerID customerID = new QCustomerID("customerID");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QCustomerID(String variable) {
        super(CustomerID.class, forVariable(variable));
    }

    public QCustomerID(Path<? extends CustomerID> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCustomerID(PathMetadata<?> metadata) {
        super(CustomerID.class, metadata);
    }

}

