FORMAT: 1A
HOST: http://localhost:8090

# Rentit-BluePrint

##PS1: Get all available plants [/api/sales/orders/GetAllPlants]

### Get all available plants [POST]
+ Request (application/json)

        {

            "rentalPeriod": {"startDate":"2016-10-10",
                             "endDate":"2016-12-12"}
        }
        
+ Response 200 (application/json)

    + Headers

            Location: /api/sales/orders/GetAllPlants

    + Body
    
                        [
              {
                "_id": 9,
                "name": "FT dumper",
                "description": "3 Tonne Front Tip Dumper",
                "price": 200,
                "status": "Delivered",
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/9"
                  }
                ]
              },
              {
                "_id": 4,
                "name": "Midi excavator",
                "description": "8 Tonne Midi excavator",
                "price": 300,
                "status": "ToDispatch",
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/4"
                  }
                ]
              },
              {
                "_id": 5,
                "name": "Maxi excavator",
                "description": "15 Tonne Large excavator",
                "price": 400,
                "status": "ToDispatch",
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/5"
                  }
                ]
              },
              {
                "_id": 12,
                "name": "Loader",
                "description": "Hewden Backhoe Loader",
                "price": 200,
                "status": null,
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/12"
                  }
                ]
              },
              {
                "_id": 11,
                "name": "FT dumper",
                "description": "8 Tonne Front Tip Dumper",
                "price": 400,
                "status": "Delivered",
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/11"
                  }
                ]
              },
              {
                "_id": 13,
                "name": "D-Truck",
                "description": "15 Tonne Articulating Dump Truck",
                "price": 250,
                "status": null,
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/13"
                  }
                ]
              },
              {
                "_id": 7,
                "name": "HS dumper",
                "description": "1.5 Tonne Hi-Swivel Dumper",
                "price": 150,
                "status": "Dispatched",
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/7"
                  }
                ]
              },
              {
                "_id": 1,
                "name": "Mini excavator",
                "description": "1.5 Tonne Mini excavator",
                "price": 150,
                "status": null,
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/1"
                  }
                ]
              },
              {
                "_id": 15,
                "name": "Mini excavator",
                "description": "3.5 Tonne Mini excavator",
                "price": 150,
                "status": null,
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/15"
                  }
                ]
              },
              {
                "_id": 8,
                "name": "FT dumper",
                "description": "2 Tonne Front Tip Dumper",
                "price": 180,
                "status": "Dispatched",
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/8"
                  }
                ]
              },
              {
                "_id": 3,
                "name": "Midi excavator",
                "description": "5 Tonne Midi excavator",
                "price": 250,
                "status": "ToDispatch",
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/3"
                  }
                ]
              },
              {
                "_id": 14,
                "name": "D-Truck",
                "description": "30 Tonne Articulating Dump Truck",
                "price": 300,
                "status": null,
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/14"
                  }
                ]
              },
              {
                "_id": 6,
                "name": "Maxi excavator",
                "description": "20 Tonne Large excavator",
                "price": 450,
                "status": "Dispatched",
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/6"
                  }
                ]
              },
              {
                "_id": 10,
                "name": "FT dumper",
                "description": "5 Tonne Front Tip Dumper",
                "price": 300,
                "status": "Delivered",
                "poID": null,
                "links": [
                  {
                    "rel": "self",
                    "href": "http://localhost:8090/api/inventory/plants/10"
                  }
                ]
              }
            ]

## PS6: Get purchase Order [/api/sales/orders/GetPOStatus/{?po_id}]

### Get  purchase Order [GET]
+ Parameters
    + po_id (required,int) 
+ Response 200 (application/json)

            {
              "_id": 101,
              "plant": {
                "_id": 9,
                "name": "FT dumper",
                "description": "3 Tonne Front Tip Dumper",
                "price": 200,
                "status": "Delivered",
                "poID": 101,
                "_links": {
                  "self": {
                    "href": "http://localhost:8090/api/inventory/plants/9"
                  }
                }
              },
              "rentalPeriod": {
                "startDate": "2016-01-01",
                "endDate": "2016-02-02"
              },
              "total": null,
              "status": "PENDING",
              "issueDate": null,
              "paymentSchedule": null,
              "contactPerson": {
                "email": "salmanlashkarara@gmail.com"
              },
              "address": {
                "address": "Liivi 2, Tartu"
              },
              "_links": {
                "self": {
                  "href": "http://localhost:8090/api/sales/orders/101"
                }
              },
              "_xlinks": [
                {
                  "href": "http://localhost:8090/api/sales/orders/101/accept",
                  "method": "DELETE",
                  "_rel": "reject"
                },
                {
                  "href": "http://localhost:8090/api/sales/orders/orders/101/accept",
                  "method": "PUT",
                  "_rel": "accept"
                },
                {
                  "href": "http://localhost:8090/api/sales/orders/orders/101",
                  "method": "PUT",
                  "_rel": "accept"
                }
              ]
            }

## PS2 & PS3: Catalog Query [/api/sales/orders/catalog/query]
### Catalog Query [POST]

+ Request (application/json)

        {
            "name":"Midi excavator",
            "rentalPeriod": {"startDate":"2016-10-10",
                     "endDate":"2016-12-12"}
        }

+ Response 200 (application/json)

    + Headers

            Location: /api/sales/orders/GetPOStatus/100

    + Body
    
            [
                {
                    "_id": 3,
                    "name": "Midi excavator",
                    "description": "5 Tonne Midi excavator",
                    "price": 250,
                    "status": "Delivered",
                    "poID": null,
                    "links": [
                      {
                        "rel": "self",
                        "href": "http://localhost:8090/api/inventory/plants/3"
                      }
                    ]
                },
                {
                    "_id": 4,
                    "name": "Midi excavator",
                    "description": "8 Tonne Midi excavator",
                    "price": 300,
                    "status": "ToDispatch",
                    "poID": null,
                    "links": [
                      {
                        "rel": "self",
                        "href": "http://localhost:8090/api/inventory/plants/4"
                      }
                    ]
                }
            ]
            

##PS4: Create Purchase Order [/api/sales/orders/CreatePO]
### Create new purchase order [POST]

+ Request (application/json)

            {  
               "_id":1,
               "plant":{
                       "_id": 9,
                       "name":"FT dumper",
                       "description":"3 Tonne Front Tip Dumper",
                       "price":200
                 },
                "rentalPeriod":{
                    "startDate":"2016-01-01",
                    "endDate":"2016-02-02"
                    
                },
                "total": 1,
                "contactPerson": {"email": "salmanlashkarara@gmail.com"},
                 "address": {"address": "Liivi 2, Tartu"}
            }

+ Response  200 (application/json)
        
    + Headers

            Location: /api/sales/orders/CreatePO

    + Body
    
                {
                    "_id": 100,
                    "plant": 
                    {
                         "_id": 9,
                         "name": "FT dumper",
                         "description": "3 Tonne Front Tip Dumper",
                         "price": 200,
                         "status": "Delivered",
                         "poID": 100,
                         "_links":
                         {
                                "self": 
                                {
                                    "href": "http://localhost:8090/api/inventory/plants/9"
                                }
                         }
                    },
                     "rentalPeriod": {
                     "startDate": "2016-01-01",
                     "endDate": "2016-02-02"
                      },
                     "total": null,
                     "status": "PENDING",
                     "issueDate": null,
                     "paymentSchedule": null,
                     "contactPerson": {
                          "email": "salmanlashkarara@gmail.com"
                      },
                     "address": 
                     {
                         "address": "Liivi 2, Tartu"
                      },
                      "_links": {
                               "self": 
                               {
                                    "href": "http://localhost:8090/api/sales/orders/100"
                               }
                      },
                      "_xlinks": [
                        {
                          "href": "http://localhost:8090/api/sales/orders/100/accept",
                          "method": "DELETE",
                          "_rel": "reject"
                        },
                        {
                          "href": "http://localhost:8090/api/sales/orders/orders/100/accept",
                          "method": "PUT",
                          "_rel": "accept"
                        },
                        {
                          "href": "http://localhost:8090/api/sales/orders/orders/100",
                          "method": "PUT",
                          "_rel": "accept"
                        }
                      ]

                }
                
## PS7: Edit Purchase Order [/api/sales/orders/EditPO/{?po_id}]

### Edit Purchase Order [POST]
+ Parameters
    + po_id (required, int) ... Purchase Order ID
+ Request (application/json)

            {  "_id":1,
                "plant":{
                "_id": 6,
                "name":"Maxi excavator",
                "description":"20 Tonne Large excavator",
                "price":450
                },
                "rentalPeriod":{
                "startDate":"2016-01-01",
                "endDate":"2016-02-02"
                },
                "total":10,
                "status": "OPEN",
                "issueDate":"2015-04-04",
                "paymentSchedule":"2015-04-04",
                "contactPerson":{"email":"salmanlashkarara@gmail.com"},
                "address":{"address": "Liivi 2, Tartu"}
            }

+ Response  200 (application/json)
        
    + Headers

            Location: /api/sales/orders/EditPO/100

    + Body
    
            {
                "_id": 100,
                "plant": {
                "_id": 6,
                "name": "Maxi excavator",
                "description": "20 Tonne Large excavator",
                "price": 450,
                "status": "Dispatched",
                "poID": 100,
                "_links": {
                  "self": {
                    "href": "http://localhost:8090/api/inventory/plants/6"
                        }
                    }
                },
                "rentalPeriod": {
                "startDate": "2016-01-01",
                "endDate": "2016-02-02"
                },
                "total": null,
                "status": "PENDING",
                "issueDate": "2015-04-04",
                "paymentSchedule": null,
                "contactPerson": {
                        "email": "salmanlashkarara@gmail.com"
                },
                "address": {
                        "address": "Liivi 2, Tartu"
                },
                "_links": {
                    "self": {
                        "href": "http://localhost:8090/api/sales/orders/100"
                            }
                },
                "_xlinks": 
                [
                    {
                        "href": "http://localhost:8090/api/sales/orders/100/accept",
                        "method": "DELETE",
                        "_rel": "reject"
                    },
                    {
                        "href": "http://localhost:8090/api/sales/orders/orders/100/accept",
                        "method": "PUT",
                        "_rel": "accept"
                    },
                    {
                        "href": "http://localhost:8090/api/sales/orders/orders/100",
                        "method": "PUT",
                        "_rel": "accept"
                    }
                ]
            }


## PS8: Purchase Order Cancelation [/api/sales/orders/POCancelation/{?po_id}]

### Purchase Order Cancelation [POST]            
+ Parameters
    + po_id (required, int) ... Purchase Order ID
+ Request (application/json)

            {  "_id":1,
               "plant":{
                     "_id": 6,
                       "name":"Maxi excavator",
                       "description":"20 Tonne Large excavator",
                       "price":450
                 },
                "rentalPeriod":{
                    "startDate":"2016-01-01",
                    "endDate":"2016-02-02"
                },
                 "total":10,
                 "status": "OPEN",
                 "issueDate":"2015-04-04",
                 "paymentSchedule":"2015-04-04",
                 "contactPerson":{"email":"salmanlashkarara@gmail.com"},
                 "address":{"address": "Liivi 2, Tartu"}
            }

+ Response  200 (application/json)
        
    + Headers

            Location: /api/sales/orders/POCancelation/100

    + Body
    
                {
                    "_id": 1,
                      "plant": {
                        "_id": 9,
                        "name": "FT dumper",
                        "description": "3 Tonne Front Tip Dumper",
                        "price": 200,
                        "status": "Delivered",
                        "poID": 1,
                        "_links": {
                          "self": {
                            "href": "http://localhost:8090/api/inventory/plants/9"
                          }
                        }
                      },
                     "rentalPeriod": {
                            "startDate": "2016-10-10",
                            "endDate": "2016-12-12"
                          },
                          "total": 1,
                          "status": "REJECTED",
                          "issueDate": "2016-12-13",
                          "paymentSchedule": "2016-12-13",
                          "contactPerson": {
                            "email": "salmanlashkarara@gmail.com"
                          },
                          "address": {
                            "address": "Liivi 2, Tartu"
                          },
                          "_links": {
                            "self": {
                              "href": "http://localhost:8090/api/sales/orders/1"
                            }
                          },
                          "_xlinks": [
                            {
                              "href": "http://localhost:8090/api/sales/orders/orders/1",
                              "method": "PUT",
                              "_rel": "accept"
                            }
                          ]
                }
                
                