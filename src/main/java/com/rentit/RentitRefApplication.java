package com.rentit;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.JsonUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.rentit.customer.repository.AuthoritiesRepository;
import com.rentit.customer.repository.UserRepository;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.invoice.InvoiceProcessor;
import com.rentit.invoice.RentalServiceGateway;
import com.rentit.invoice.SendRecieveInvoice;
import com.rentit.sales.application.dto.QueryDTO;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.SpringVersion;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.http.Http;
import org.springframework.integration.dsl.mail.Mail;
import org.springframework.integration.dsl.support.Transformers;
import org.springframework.integration.support.json.Jackson2JsonObjectMapper;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

@EntityScan(basePackageClasses = { RentitRefApplication.class, Jsr310JpaConverters.class })

@SpringBootApplication
@IntegrationComponentScan
@Component
@EnableHypermediaSupport(type=EnableHypermediaSupport.HypermediaType.HAL)
public class RentitRefApplication {

    @Configuration
    static class ObjectMapperCustomizer {
        @Autowired @Qualifier("_halObjectMapper")
        private ObjectMapper springHateoasObjectMapper;
 
 //       @Bean(name = "objectMapper")
//        ObjectMapper objectMapper() {
//            return springHateoasObjectMapper
//                    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
//                    .configure(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
//                    .configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
//                    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
//                    .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
//                    .registerModules(new JavaTimeModule());
//        }
        @Bean
        public RestTemplate restTemplate() {
            RestTemplate _restTemplate = new RestTemplate();
            List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
            messageConverters.add(new MappingJackson2HttpMessageConverter(springHateoasObjectMapper));
            _restTemplate.setMessageConverters(messageConverters);
            return _restTemplate;
        }
    }
 
 
    public static void main(String[] args) {
        SpringApplication.run(RentitRefApplication.class, args);
 
    }
} 

	

