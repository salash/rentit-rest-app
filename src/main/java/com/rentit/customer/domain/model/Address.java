package com.rentit.customer.domain.model;

import java.time.LocalDate;

import javax.persistence.Embeddable;

import com.rentit.common.domain.model.BusinessPeriod;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

@Embeddable
@Value
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@AllArgsConstructor(staticName = "of")
public class Address {
	String address;

}
