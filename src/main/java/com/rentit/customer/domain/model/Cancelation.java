package com.rentit.customer.domain.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import com.rentit.sales.domain.model.PurchaseOrderID;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class Cancelation {
	
	@EmbeddedId
	CancelationID id;
	
	@Embedded
	@AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="customer_id"))})
	CustomerID customerId;
	

	@Embedded
	@AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="PO_id"))})
	PurchaseOrderID poID;
	
	
	
	public static Cancelation of(CancelationID cId,CustomerID customerId, PurchaseOrderID poID ){
		Cancelation ca=new Cancelation();
		ca.setCustomerId(customerId);
		ca.setPoID(poID);
		return ca;
		
	}
}
