package com.rentit.customer.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.domain.model.PlantInventoryEntryID;
import com.rentit.inventory.domain.model.PlantReservationID;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.model.PurchaseOrderID;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class Customer {
	
	@EmbeddedId
	CustomerID id;
	String name;
	String address;
	String phone;
	
	public static Customer of(CustomerID id, String name,String address, String phone) {
		Customer cu = new Customer();
		cu.id = id;
		cu.name = name;
		cu.address=address;
		cu.phone=phone;
		
		
		return cu;
		}


}







