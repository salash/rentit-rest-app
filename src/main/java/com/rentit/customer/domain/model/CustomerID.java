package com.rentit.customer.domain.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.rentit.sales.domain.model.PurchaseOrderID;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

@Embeddable
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class CustomerID implements Serializable{
	Long id;

}
