package com.rentit.customer.domain.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class User {

	@EmbeddedId
	UserID id;
	String name;
	String address;
	String phone;
	String password;
	String username;
	boolean enabled;
	
	public static User of(UserID id, String name,String address, String phone, String username, String pass) {
		User cu = new User();
		cu.id = id;
		cu.name = name;
		cu.address=address;
		cu.phone=phone;
		cu.username= username;
		cu.password= pass;
		cu.enabled=true;
		
		return cu;
		}
}
