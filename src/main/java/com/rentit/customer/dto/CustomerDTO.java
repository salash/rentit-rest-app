package com.rentit.customer.dto;


import com.rentit.common.rest.ResourceSupport;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(force = true)
public class CustomerDTO extends ResourceSupport{
	
	Long       _id;
	String    name;
	String address;
	String   phone;
	String   pass;
	String confirmPass;

}
