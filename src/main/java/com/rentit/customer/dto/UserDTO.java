package com.rentit.customer.dto;

import com.rentit.customer.domain.model.UserID;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(force = true)
public class UserDTO {
	
	long id;
	String name;
	String address;
	String phone;
	String password;
	String userName;
	String confirmPass;
}
