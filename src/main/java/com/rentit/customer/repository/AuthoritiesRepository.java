package com.rentit.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rentit.customer.domain.model.Authorities;
import com.rentit.customer.domain.model.AuthorityID;
import com.rentit.customer.domain.model.User;
import com.rentit.customer.domain.model.UserID;

public interface AuthoritiesRepository extends JpaRepository<Authorities,AuthorityID>{

}
