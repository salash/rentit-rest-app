package com.rentit.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rentit.customer.domain.model.Customer;
import com.rentit.customer.domain.model.CustomerID;
import com.rentit.customer.domain.model.User;
import com.rentit.customer.domain.model.UserID;

public interface UserRepository extends JpaRepository<User,UserID>{

}
