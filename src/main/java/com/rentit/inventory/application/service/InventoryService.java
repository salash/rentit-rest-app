package com.rentit.inventory.application.service;

import com.rentit.common.application.dto.BusinessPeriodDTO;
import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.dto.PlantReservationDTO;
import com.rentit.inventory.domain.model.PlantDeliveryStatus;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryEntryID;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.domain.repository.InventoryRepository;
import com.rentit.inventory.domain.repository.PlantReservationRepository;
import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.inventory.infrastructure.idgeneration.InventoryIdentifierGenerator;
import com.rentit.invoice.SendRecieveInvoice;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.application.service.SalesService;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.model.PurchaseOrderID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class InventoryService {
    @Autowired
    InventoryIdentifierGenerator identifierGenerator;

    @Autowired
    PlantInventoryEntryAssembler entryAssembler;
    @Autowired
    PlantReservationAssembler reservationAssembler;

    @Autowired
    InventoryRepository inventoryRepository;
    @Autowired
    PlantReservationRepository reservationRepository;
    
    @Autowired
    SalesService salesServic; 
    
    @Autowired
     SendRecieveInvoice InvoiceService;

    public List<PlantInventoryEntryDTO> findAllPlantInventoryEntries() {
        return entryAssembler.toResources(inventoryRepository.findAll());
    }

    public List<PlantInventoryEntryDTO> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
    	List<PlantInventoryEntry> plants = inventoryRepository.findAvailablePlants(name,BusinessPeriod.of(startDate, endDate));
    	return entryAssembler.toResources(plants);
    }   
    
    public List<PlantInventoryEntryDTO> findAllAvailablePlants( LocalDate startDate, LocalDate endDate) {
    	List<PlantInventoryEntry> plants = inventoryRepository.findAllAvailablePlants(BusinessPeriod.of(startDate, endDate));
    	return entryAssembler.toResources(plants);
    }
    
    
    

    public List<PlantInventoryEntryDTO> findPlantsByName(String name) {
        return entryAssembler.toResources(inventoryRepository.findByNameContainingIgnoreCase(name));
    }

    public PlantInventoryEntry findPlant(PlantInventoryEntryDTO plant) {
       // return inventoryRepository.findOne(PlantInventoryEntryID.of(plant.get_id()));
         PlantInventoryEntryID plant_id = PlantInventoryEntryID.of(plant.get_id());
    	 return this.findPlantByID(plant_id);
    }

    public PlantInventoryEntryDTO findPlant(PlantInventoryEntry plant) {
        return entryAssembler.toResource(inventoryRepository.findOne(plant.getId()));
    }

    public PlantInventoryEntryDTO findPlant(PlantInventoryEntryID plantID) throws PlantNotFoundException {
       // PlantInventoryEntry entry = inventoryRepository.findOne(plantID);
      PlantInventoryEntry entry = inventoryRepository.findByid(plantID);
        if (entry == null)
            throw new PlantNotFoundException("Requested plant does not exist!");
        return entryAssembler.toResource(entry);
    }

    public PlantReservationDTO createPlantReservation(PlantInventoryEntryDTO plant, BusinessPeriodDTO periodDTO) throws PlantNotFoundException {
        BusinessPeriod period = BusinessPeriod.of(periodDTO.getStartDate(), periodDTO.getEndDate());
        List<PlantInventoryItem> list = inventoryRepository.findAvailableInventoryItems(plant.getName(), period);

        if (list.size() == 0)
            throw new PlantNotFoundException("Requested plant is not available");

        PlantReservation reservation = PlantReservation.of(
                identifierGenerator.nextPlantReservationID(),
                period,
                list.get(0).getId()
                
        		);

        reservationRepository.save(reservation);

        return reservationAssembler.toResource(reservation);
    }
    
    public PlantInventoryEntryDTO findPlantFullRepresentation(PlantInventoryEntryDTO plant) {
        PlantInventoryEntryID id = entryAssembler.resolveId(plant.getId());
        return entryAssembler.toResource(inventoryRepository.findOne(id));
    }

	public boolean changePlantStatusToBeDelivered(Long id, Long POId) {
		
		PlantInventoryEntry plant = inventoryRepository.findOne(PlantInventoryEntryID.of(id));
	    plant.setStatus(PlantDeliveryStatus.ToDispatch); 
	    inventoryRepository.save(plant);
	    
	// /*   PurchaseOrderID po_id = plant.getPO_id();
	    salesServic.changePOStatusToAccepted(POId);
	  // */
	    
	    return true;
	}

	public List<PlantInventoryEntryDTO> findAllPlantInventoryEntriesWhichVerifiedToDeliver() {
		  return entryAssembler.toResources(inventoryRepository.findByStatus(PlantDeliveryStatus.ToDispatch));
	//	return null;
	}

	public boolean changePlantStatusToDispatched(Long id) {
		PlantInventoryEntry plant = inventoryRepository.findOne(PlantInventoryEntryID.of(id));
	    plant.setStatus(PlantDeliveryStatus.Dispatched); 
	    inventoryRepository.save(plant);
	    
	    return true;
	}

	public List<PlantInventoryEntryDTO> findAllDispatchedPlantInventoryEntries() {
		 return entryAssembler.toResources(inventoryRepository.findByStatus(PlantDeliveryStatus.Dispatched));
	
	}

	public boolean changePlantStatusToDelivered(Long id) {
		PlantInventoryEntry plant = inventoryRepository.findOne(PlantInventoryEntryID.of(id));
	    plant.setStatus(PlantDeliveryStatus.Delivered); 
	    inventoryRepository.save(plant);
	    
	    return true;
	}
	
/*	public boolean changePlantStatusToDispatched(Long id) {
		PlantInventoryEntry plant = inventoryRepository.findOne(PlantInventoryEntryID.of(id));
	    plant.setStatus(PlantDeliveryStatus.Delivered); 
	    inventoryRepository.save(plant);
	    
	    return true;
	}
	
	*/
	

	public boolean changePlantStatusToReturned(Long id) {
		PlantInventoryEntry plant = inventoryRepository.findOne(PlantInventoryEntryID.of(id));
	    plant.setStatus(PlantDeliveryStatus.RejectedByCustomer); 
	    inventoryRepository.save(plant);
		return false;
	}


	public boolean changePlantStatusToReturnedToDepo(Long id, Long POId) throws Exception {
		PlantInventoryEntry plant = inventoryRepository.findOne(PlantInventoryEntryID.of(id));
		
		 PurchaseOrderDTO po = salesServic.findPurchaseOrder(PurchaseOrderID.of(POId));
		 System.out.println("The plant returned and an email is sent to:"+po.getContactPerson().getEmail());
		 InvoiceService.sendInvoice(po.getContactPerson().getEmail());
		
		 plant.setStatus(PlantDeliveryStatus.Returned); 
	     inventoryRepository.save(plant);
		
	     return true;
	}
	  public PlantInventoryEntry findPlantByID(PlantInventoryEntryID plant_id) {
	        return inventoryRepository.findByid(plant_id);
	    }
    
}
