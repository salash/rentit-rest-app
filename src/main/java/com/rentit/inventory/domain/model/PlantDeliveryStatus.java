package com.rentit.inventory.domain.model;

public enum PlantDeliveryStatus {
	
	Dispatched, Delivered, RejectedByCustomer, Returned, ToDispatch,

}
