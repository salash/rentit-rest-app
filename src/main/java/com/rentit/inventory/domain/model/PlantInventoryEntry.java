package com.rentit.inventory.domain.model;

import lombok.*;

import javax.persistence.*;

import com.rentit.sales.domain.model.PurchaseOrderID;

import java.math.BigDecimal;

@Entity
@Data @ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@AllArgsConstructor(staticName = "of")
public class PlantInventoryEntry {
    @EmbeddedId
    PlantInventoryEntryID id;
    String name;
    String description;
    
    @Column(precision = 8, scale = 2)
    BigDecimal price;
    /*
    @Embedded
    @AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="PO_id"))})
    PurchaseOrderID   PO_id;
    */
    PlantDeliveryStatus status;
}
