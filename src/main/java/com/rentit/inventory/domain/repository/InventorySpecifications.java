package com.rentit.inventory.domain.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;
import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.domain.model.*;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrderID;
import com.rentit.sales.domain.model.QPurchaseOrder;

public class InventorySpecifications {
    static QPlantInventoryItem plantInvItem = QPlantInventoryItem.plantInventoryItem;
    static QPlantInventoryEntry plantInvEntry = QPlantInventoryEntry.plantInventoryEntry;
    static QPlantReservation reservation = QPlantReservation.plantReservation;
    static QPurchaseOrder purchaseOrder= QPurchaseOrder.purchaseOrder;

    public static BooleanExpression plantInvItemIsAvailableFor(BusinessPeriod period) {
        return plantInvItem.id.notIn(new JPASubQuery()
                .from(reservation)
                .where(reservation.schedule.endDate.goe(period.getStartDate()),
                        reservation.schedule.startDate.loe(period.getEndDate()))
                .list(reservation.plant));
    }

    public static BooleanExpression findPurchaseOrder( PurchaseOrderID id ){
    	
    	
    	BooleanExpression result = purchaseOrder.id.eq(id);	
    	
		return result; 
    	
    	
    } 
    
    public static void updatePO(PurchaseOrderID id){
    	/*
    	new JPAUpdateClause(, purchaseOrder).where(purchaseOrder.id.eq(id))//purchaseOrder.status.eq("OPEN"))
        .set(purchaseOrder.status, POStatus.REJECTED)
        .execute();
    	
    */
    	
    	
    }
    
    public static BooleanExpression entryHasAvailableItemFor(BusinessPeriod period, EquipmentCondition condition) {
        return plantInvEntry.id.in(new JPASubQuery().from(plantInvItem)
                .where(plantInvItemIsAvailableFor(period).and(plantInvItem.condition.eq(condition)))
                .list(plantInvItem.plantInfo));
    }

    public static BooleanExpression plantNameContains(String keyword) {
        return plantInvEntry.name.lower().contains(keyword.toLowerCase());
    }
}