package com.rentit.invoice;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.stereotype.Service;

import com.rentit.sales.application.service.SalesService;
import com.rentit.sales.domain.model.PurchaseOrderID;
@Configuration
@Service
@IntegrationComponentScan
public class InvoiceProcessor {
	 @Autowired
	 SalesService salesService;
	
	 public String extractUrl(String invoice){
	        Pattern p = Pattern.compile("<purchaseOrderHRef>(.*?)</purchaseOrderHRef>");
	        Matcher m = p.matcher(invoice);
	        while(m.find())
	        {
	            return m.group(1);
	        }
	        return "";
	    }
	
	 public Long getUrlID(String url){
	        return Long.valueOf(url.substring(url.lastIndexOf("/")+1, url.length()));
	    }
    public void processInvoice(String invoice) {
        System.out.println("About to process invoice:\n" + invoice);
        String poURL = extractUrl(invoice);
        Long poID = getUrlID(poURL);
        System.out.println("PO ID is: "+poID);
        salesService.changePOToPaid(PurchaseOrderID.of(poID));
        
        
        
    }

    public String extractInvoice(MimeMessage msg) throws Exception {
        Multipart multipart = (Multipart) msg.getContent();
        for (int i = 0; i < multipart.getCount(); i++) {
            BodyPart bodyPart = multipart.getBodyPart(i);
            if (bodyPart.getContentType().contains("xml") && bodyPart.getFileName().startsWith("invoice"))
                return IOUtils.toString(bodyPart.getInputStream(), "UTF-8");
        }
        throw new Exception("Oops ... no invoice found in email");
    }
}
