package com.rentit.invoice;
///*

import java.util.List;

import javax.mail.internet.MimeMessage;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

import com.rentit.inventory.application.dto.*;
import com.rentit.sales.application.dto.QueryDTO;


@MessagingGateway
public interface RentalServiceGateway {
  @Gateway(requestChannel = "queryPlantCatalogChannel")
  List<PlantInventoryEntryDTO> queryPlantCatalog(QueryDTO query);
  @Gateway(requestChannel = "sendInvoiceChannel")
  public void sendInvoice(MimeMessage msg);
}

//*/