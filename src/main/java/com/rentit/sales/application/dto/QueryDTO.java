package com.rentit.sales.application.dto;



import com.rentit.common.application.dto.BusinessPeriodDTO;

import lombok.Data;

@Data
public class QueryDTO {
    String name;
    BusinessPeriodDTO rentalPeriod;
}