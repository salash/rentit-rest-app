package com.rentit.sales.application.service;

import com.rentit.common.application.dto.BusinessPeriodDTO;
import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.rest.ExtendedLink;
import com.rentit.customer.domain.model.Address;
import com.rentit.customer.dto.AddressDTO;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.service.InventoryService;
import com.rentit.inventory.application.service.PlantInventoryEntryAssembler;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryEntryID;
import com.rentit.sales.application.dto.ContactPersonDTO;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.rest.PurchaseOrderRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.PUT;

import java.util.ArrayList;
import java.util.List;


@Service
public class PurchaseOrderAssembler extends ResourceAssemblerSupport<PurchaseOrder, PurchaseOrderDTO> {
    @Autowired
    InventoryService inventoryService;
    @Autowired
    PlantInventoryEntryAssembler entryAssembler;

    public PurchaseOrderAssembler() {
        super(PurchaseOrderRestController.class, PurchaseOrderDTO.class);
    }

    @Override
    public PurchaseOrderDTO toResource(PurchaseOrder purchaseOrder) {
    	 try {
    	PurchaseOrderDTO dto = createResourceWithId(purchaseOrder.getId().getId(), purchaseOrder);
         try {
        	 
            //dto.setPlant(inventoryService.findPlant(purchaseOrder.getPlant()));
	         PlantInventoryEntryID plantID = purchaseOrder.getPlant();
	         PlantInventoryEntry plant = inventoryService.findPlantByID(plantID);
	         PlantInventoryEntryDTO plantDTO = entryAssembler.toResource(plant);
	         plantDTO.setPoID(purchaseOrder.getId().getId());
        	 dto.setPlant(plantDTO);
             dto.set_id(purchaseOrder.getId().getId());
             dto.setRentalPeriod(BusinessPeriodDTO.of(purchaseOrder.getRentalPeriod().getStartDate(), purchaseOrder.getRentalPeriod().getEndDate()));
             dto.setTotal(purchaseOrder.getTotal());
             dto.setStatus(purchaseOrder.getStatus());
             dto.setAddress(AddressDTO.of(purchaseOrder.getAddress().getAddress()));
             dto.setContactPerson(ContactPersonDTO.of(purchaseOrder.getContactPerson().getEmail()));
             dto.setIssueDate(purchaseOrder.getIssueDate());
             dto.setPaymentSchedule(purchaseOrder.getPaymentSchedule());
         
         } catch (Exception e) {
        	 System.out.println("Plant not found");
           // throw new IllegalArgumentException(e.getMessage());
        }//*/

   
       // /*     
       
        	
//            switch (purchaseOrder.getStatus()) {
//            
//	            case OPEN:
////	                dto.add(new ExtendedLink(
////	                        linkTo(methodOn(PurchaseOrderRestController.class)
////	                          .createPO(dto)).toString(),
////	                        "create", POST));
//	                dto.add(new ExtendedLink(
//	                        linkTo(methodOn(PurchaseOrderRestController.class)
//	                          .ClosePO(dto.get_id())).toString(),
//	                        "close", DELETE));
//	                dto.add(new ExtendedLink(
//	                        linkTo(methodOn(PurchaseOrderRestController.class)
//	                          .extendRentalPeriodPO(dto.get_id())).toString(),
//	                        "extendRentalPeriod", DELETE));
//	                
//	                
//                        
//	            case PENDING:   
//                    dto.add(new ExtendedLink(
//                              linkTo(methodOn(PurchaseOrderRestController.class)
//                                .RejectingPO(dto.get_id())).toString(),
//                              "reject", DELETE)); 
//                    dto.add(new ExtendedLink(
//                            linkTo(methodOn(PurchaseOrderRestController.class)
//                              .acceptPO(dto.get_id())).toString(),
//                            "accept", PUT));  
//	            
//	            case  REJECTED:
//	                 dto.add(new ExtendedLink(
//	                            linkTo(methodOn(PurchaseOrderRestController.class)
//	                              .reSubmitPO(dto.get_id())).toString(),
//	                            "accept", PUT));
//                    
//         /*       case PENDING_EXTENTION:
//                	 dto.add(new ExtendedLink(
//	                            linkTo(methodOn(PurchaseOrderRestController.class)
//	                              .rejectRPExtention(dto.get_id(), )).toString(),
//	                            "accept", PUT));       
//                	
//           */     	
//                	
//                    
//                    break;
//               default: break;
//            }
            return dto;
    	 } catch (Exception e) {
        	return null;
        }
     //  */
        
       
       
    }
   
    public ArrayList<PurchaseOrderDTO> toResource(ArrayList<PurchaseOrder> purchaseOrderList) {
    	
    	ArrayList<PurchaseOrderDTO> DTOList= new ArrayList<PurchaseOrderDTO>();
    	for(int i=0;i<purchaseOrderList.size();i++)
    		DTOList.add(toResource(purchaseOrderList.get(i)));
    		
    	return DTOList;
    	
    }    

}
