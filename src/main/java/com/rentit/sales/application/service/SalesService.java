package com.rentit.sales.application.service;

import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.common.domain.validation.BusinessPeriodValidator;
import com.rentit.customer.domain.model.Address;
import com.rentit.customer.domain.model.CustomerID;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.dto.PlantReservationDTO;
import com.rentit.inventory.application.service.InventoryService;
import com.rentit.inventory.application.service.PlantInventoryEntryAssembler;
import com.rentit.inventory.domain.model.PlantDeliveryStatus;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryEntryID;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.inventory.domain.model.PlantReservationID;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.domain.model.ContactPerson;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.model.PurchaseOrderID;
import com.rentit.sales.domain.repository.PurchaseOrderRepository;
import com.rentit.sales.domain.validation.ContactPersonValidator;
import com.rentit.sales.domain.validation.PurchaseOrderValidator;
import com.rentit.sales.infrastructure.idgeneration.SalesIdentifierGenerator;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.springframework.validation.DataBinder;

@Service
public class SalesService {
    @Autowired
    InventoryService inventoryService;
    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;
    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;
    
    @Autowired
    PlantInventoryEntryRepository plantInventoryEnteryRepository;
    
    @Autowired
    PlantInventoryEntryAssembler entryAssembler;
    
    
    @Autowired
    SalesIdentifierGenerator identifierGenerator;
    
    public PlantInventoryEntry AddNewPlantInventoryEntery(PlantInventoryEntry pie){
    	return plantInventoryEnteryRepository.save(pie);
    }
    
    public PurchaseOrderDTO createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO/*,Long cu_id*/) throws BindException, PlantNotFoundException {
        
    //	CustomerID CU_ID=   CustomerID.of(cu_id);
    	try{
    	PlantInventoryEntry plant = inventoryService.findPlant(purchaseOrderDTO.getPlant());
        
    	PurchaseOrder po = PurchaseOrder.of(
                identifierGenerator.nextPurchaseOrderID(),
                plant.getId(),
                BusinessPeriod.of(purchaseOrderDTO.getRentalPeriod().getStartDate(),purchaseOrderDTO.getRentalPeriod().getEndDate()),
                Address.of(purchaseOrderDTO.getAddress().getAddress()),
                ContactPerson.of(purchaseOrderDTO.getContactPerson().getEmail())
        );
    	po.setIssueDate(LocalDate.now());
    	po.setTotal(purchaseOrderDTO.getTotal());
    	
    	po.setPaymentSchedule(LocalDate.now());
    /*    plant.setPO_id(po.getId());  */
        plantInventoryEnteryRepository.save(plant);
        
        DataBinder binder = new DataBinder(po);
        binder.addValidators(new PurchaseOrderValidator(new BusinessPeriodValidator(), new ContactPersonValidator()));
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new BindException(binder.getBindingResult());

        purchaseOrderRepository.save(po);
        /*
        PlantReservationDTO reservationDTO = inventoryService.createPlantReservation(purchaseOrderDTO.getPlant(), purchaseOrderDTO.getRentalPeriod());
///*
        po.confirmReservation(
                PlantReservationID.of(reservationDTO.get_id()),
                plant.getPrice()); // Shouldn't we also pass the reservation schedule as a parameter to check if it matches rental period?
*/
        binder = new DataBinder(po);
        binder.addValidators(new PurchaseOrderValidator(new BusinessPeriodValidator(), new ContactPersonValidator()));
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new BindException(binder.getBindingResult());

        purchaseOrderRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    	}
  /*  	catch(PlantNotFoundException e){
    		
    	   // return purchaseOrderAssembler.toResource(null);
    	  //  System.out.println("There is no plant");
    	    return null;
    	} */
    	catch(NullPointerException e){
    		
     	   // return purchaseOrderAssembler.toResource(null);
     	    System.out.println("There is no plant");
     	    return null;
     	}
    }

    public PurchaseOrderDTO findPurchaseOrder(PurchaseOrderID id) {
    	PurchaseOrder po = purchaseOrderRepository.findOne(id);
        return purchaseOrderAssembler.toResource(po);
    }
    
    public  ArrayList<PurchaseOrderDTO> findByStatus(POStatus open) {
    	ArrayList<PurchaseOrder> po = purchaseOrderRepository.findByStatus(open);
    	return purchaseOrderAssembler.toResource(po);
	}
    
    public void ChangStatusOfPurchaseOrder(PurchaseOrderID id){//,POStatus p) {
    	
        PurchaseOrder po = purchaseOrderRepository.findById(id);
        changePOStatus(po.getId().getId());
    }
    
    public void changePOToPaid(PurchaseOrderID POid){
    	
    	PurchaseOrder po = purchaseOrderRepository.findById(POid);
    	po.setPaid(true);
    	purchaseOrderRepository.save(po);
    }
    
 /*   
public void RejectPurchaseOrder(PurchaseOrderID id) {
    	
	   PurchaseOrder po = purchaseOrderRepository.findOne(id);
	   po.setStatus(POStatus.REJECTED);
	   purchaseOrderRepository.save(po);
      
    }
   */ 
    

	public PurchaseOrderDTO RejectPurchaseOrder(PurchaseOrderID po) {
		
		PurchaseOrder exist_PO=      purchaseOrderRepository.findById(po);
	//	PurchaseOrder exist_PO=      purchaseOrderRepository.findOne(po);
	/*	PurchaseOrder new_PO= PurchaseOrder.of(identifierGenerator.nextPurchaseOrderID(),
				                               exist_PO.getPlant(), 
				                               exist_PO.getRentalPeriod());   */
		
		
		if(exist_PO != null) {
			exist_PO.setStatus(POStatus.REJECTED);
			purchaseOrderRepository.save(exist_PO);
		//	purchaseOrderRepository.RejectPO(po.getId());
			 return purchaseOrderAssembler.toResource(exist_PO);
		
		}
		else
			return null;
	}

	public void PendingPurchaseOrder(PurchaseOrderID of) {
		purchaseOrderRepository.PendingPO(of.getId());
		
	}

	public void ClosePurchaseOrder(PurchaseOrderID of) {
		purchaseOrderRepository.ClosePO(of.getId());
	}

	public PurchaseOrderDTO getPurchaseOrder(PurchaseOrderID of) {
		
		  PurchaseOrder  po=   purchaseOrderRepository.getPO(of.getId());
		  return	purchaseOrderAssembler.toResource(po); 
		
	}

	public ArrayList<PurchaseOrder> findExpiredPOs(LocalDate localDate) {
		ArrayList<PurchaseOrder> result=new ArrayList<PurchaseOrder>();
		List<PurchaseOrder> POList= purchaseOrderRepository.findAll();
		for(PurchaseOrder po: POList) {
			 System.out.println("localDate is:  "+localDate);
			if(po.getRentalPeriod().getEndDate().isBefore(localDate))
			{	
			 //   System.out.println("Status is:  "+po.getStatus());
			    result.add(po);
			}
		}
		return result;
	}

	public void checkCancelationPosibility(Long id) throws PlantNotFoundException {
		try{
		PurchaseOrder PO=      purchaseOrderRepository.findOne(PurchaseOrderID.of(id));

		PlantInventoryEntryDTO plant = inventoryService.findPlant(PO.getPlant());
		
		if(plant.getStatus()!=PlantDeliveryStatus.Dispatched)	
			PO.setCanceled(true);
		else
			PO.setCanceled(false);
		purchaseOrderRepository.save(PO);
		}catch(Exception e){
			System.out.println("PO not found for cancelation");
			
		}
                           	 
	}

	public void changePOStatus(Long id) {
		PurchaseOrder po = purchaseOrderRepository.findById(PurchaseOrderID.of(id)  );
	    po.setStatus(POStatus.CLOSED);
		purchaseOrderRepository.save(po);
	
	}
	public void changePOStatusToAccepted(Long id) {
		PurchaseOrder po = purchaseOrderRepository.findById(PurchaseOrderID.of(id)  );
	    po.setStatus(POStatus.Accpted);
		purchaseOrderRepository.save(po);
	
	}

	public PurchaseOrderDTO EditePO(PurchaseOrderDTO editedpo/*, Long cu_id*/) {
	  try{	
		//PurchaseOrderDTO po = findPurchaseOrder(PurchaseOrderID.of(po_id));
	     PurchaseOrder po = purchaseOrderRepository.findById(PurchaseOrderID.of(editedpo.get_id()));
	     po.setCanceled( false );
	 //    po.setCustomerId(CustomerID.of(cu_id));
	     
	     String address= editedpo.getAddress().getAddress();
	     Address siteAddress=Address.of(address);
	     po.setAddress(siteAddress);
	     
	     po.setContactPerson(ContactPerson.of(editedpo.getContactPerson().getEmail()));
	     po.setIssueDate(editedpo.getIssueDate());
	     po.setPaid(false);
	     
	     po.setStatus(POStatus.PENDING);
		     
	      PlantInventoryEntryID plantId = PlantInventoryEntryID.of(editedpo.getPlant().get_id());
	      PlantInventoryEntry newPlant = inventoryService.findPlantByID(plantId);
	  /*    newPlant.setPO_id(po.getId()); */
	     po.setPlant(newPlant.getId());
	     
	     
	     BusinessPeriod rentalPeriod = BusinessPeriod.of(editedpo.getRentalPeriod().getStartDate(),
	    		                                         editedpo.getRentalPeriod().getEndDate());
	     po.setRentalPeriod(rentalPeriod);
	    
	     purchaseOrderRepository.save(po);
	     return purchaseOrderAssembler.toResource(po);
	  }catch(Exception e){
		  System.out.println("No PO found");
		  return null;
	  }
	  }
	
///*
	public ArrayList<String> GetAllDebtorsEmail()
	{
		ArrayList<String> emails = purchaseOrderRepository.getAllDebtorsEmail();
		return emails; 
	}
//*/
/*	public PurchaseOrderDTO rejectRPExtension(PurchaseOrderID of, Long eid) {
		  PurchaseOrder  po=   purchaseOrderRepository.getPO(of.getId());
		  po.rejectRPExtension(eid);
		return null;
	} */

	public List<PlantInventoryEntryDTO> findDeliveredPlants() {
		ArrayList<PlantInventoryEntryDTO> plantList=new ArrayList<PlantInventoryEntryDTO>();
		ArrayList<PurchaseOrder> poAcceptedList=purchaseOrderRepository.findByStatus(POStatus.Accpted);
		for(int i=0;i<poAcceptedList.size(); i++){
			PlantInventoryEntryID plantId = poAcceptedList.get(i).getPlant();
			PlantInventoryEntry plant = inventoryService.findPlantByID(plantId);
			if(plant.getStatus()==PlantDeliveryStatus.Delivered){
				PlantInventoryEntryDTO plantDTO = entryAssembler.toResource(plant);	
				plantDTO.setPoID(poAcceptedList.get(i).getId().getId());
				plantList.add(plantDTO);
			}
		}
			
		return plantList;
	}
}
