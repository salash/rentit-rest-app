package com.rentit.sales.domain.model;

import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.customer.domain.model.Address;
import com.rentit.customer.domain.model.Comment;
import com.rentit.customer.domain.model.Customer;
import com.rentit.customer.domain.model.CustomerID;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryEntryID;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.domain.model.PlantReservationID;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
//@Getter
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class PurchaseOrder {
    @EmbeddedId
    PurchaseOrderID id;

    @Embedded
    BusinessPeriod rentalPeriod;

    @Embedded
    Address address; 
    
    
    @Embedded
    ContactPerson contactPerson; 
    
  //  @Embedded
  //  @AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="customer_id"))})
  //  CustomerID customerId;
    
    LocalDate issueDate;
    LocalDate paymentSchedule;

    @Column(precision = 8, scale = 2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    POStatus status;
    
   
    
    boolean Canceled;
    
    boolean paid;
 ///*   
    @Embedded
    @AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="plant_id"))})
    PlantInventoryEntryID plant;
   

    @ElementCollection
    @AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="reservation_id"))})
    List<PlantReservationID> reservations = new ArrayList<>();


    public static PurchaseOrder of(PurchaseOrderID id, 
    		                       PlantInventoryEntryID plant,
    		                       BusinessPeriod period,
    		                       Address address,
    		                       ContactPerson contactPerson) {
        PurchaseOrder po = new PurchaseOrder();
        po.id = id;
        po.plant = plant;
        po.rentalPeriod = period;
        po.status = POStatus.PENDING;
        po.address=address;
        po.contactPerson=contactPerson;
        return po;
    }

    public void confirmReservation(PlantReservationID reservation, BigDecimal plantPrice) {
        reservations.add(reservation);
        total = plantPrice.multiply(BigDecimal.valueOf(rentalPeriod.numberOfWorkingDays()));
        status = POStatus.OPEN;
    }
    public void acceptPurchaseOrder() {
    	status = POStatus.OPEN;
    }
    
   // */
}
