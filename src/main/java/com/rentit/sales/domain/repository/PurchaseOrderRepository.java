package com.rentit.sales.domain.repository;

import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.model.PurchaseOrderID;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, PurchaseOrderID>,
QueryDslPredicateExecutor<PurchaseOrder>{
    
	@Modifying
	@Query(value ="update PURCHASE_ORDER   set status='REJECTED'   where id=?1", nativeQuery = true)
	void RejectPO(Long id);
	@Modifying
	@Query(value ="update PURCHASE_ORDER   set status='PENDING'   where id=?1", nativeQuery = true)
	void PendingPO(Long id);
	@Modifying
	@Query(value ="update PURCHASE_ORDER   set status='CLOSED'   where id=?1", nativeQuery = true)
	void ClosePO(Long of);
	
	@Query(value ="SELECT * FROM PURCHASE_ORDER where id=?1", nativeQuery = true)
	PurchaseOrder getPO(Long id);
	
	
	ArrayList<PurchaseOrder> findByStatus(POStatus open);
	
	PurchaseOrder findById(PurchaseOrderID id);
	///*
	//@Modifying
	@Query(value ="SELECT email FROM PURCHASE_ORDER where status='CLOSED' and paid='false' ", nativeQuery = true)
	 ArrayList<String> getAllDebtorsEmail();
	
	//*/
}
