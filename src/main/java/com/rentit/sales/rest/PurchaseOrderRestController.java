package com.rentit.sales.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.service.InventoryService;
import com.rentit.inventory.domain.repository.InventorySpecifications;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.application.service.SalesService;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.model.PurchaseOrderID;
import com.rentit.sales.web.CatalogQueryDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api/sales/orders")
public class PurchaseOrderRestController {
    @Autowired
    SalesService salesService;
    @Autowired
    ObjectMapper mapper;

    @Autowired
    InventoryService InventoryService;
/*
    @RequestMapping(method = POST, path = "/plants")     //PS1 
    @Secured({"ROLE_SiteEngineerr","ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
    public ResponseEntity< List<PlantInventoryEntryDTO>>  getAllAvailablePlants(@RequestBody CatalogQueryDTO query) throws Exception {
    List<PlantInventoryEntryDTO> availablePlants = InventoryService.findAllAvailablePlants(
					            query.getRentalPeriod().getStartDate(),
					            query.getRentalPeriod().getEndDate());
    
    	System.out.println("GetAllPlants:  ");
        
        HttpHeaders headers = new HttpHeaders();
        try{
        return new ResponseEntity< List<PlantInventoryEntryDTO>>(availablePlants, headers, HttpStatus.CREATED); 
        }
        catch(Exception e){
        	System.out.println("Failed to make plant");
        	return null;
        }
    }
 */
    
//    @RequestMapping(method = POST, path = "/catalog/query")  // PS2, PS3
//    @Secured({"ROLE_SiteEngineerr","ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
//    public ResponseEntity<List<PlantInventoryEntryDTO>> queryPlantCatalog(@RequestBody CatalogQueryDTO query) {
//    
//    	List<PlantInventoryEntryDTO> availablePlants = InventoryService.findAvailablePlants(
//                query.getName(),
//                query.getRentalPeriod().getStartDate(),
//                query.getRentalPeriod().getEndDate());
//    		
//    	  HttpHeaders headers = new HttpHeaders();
//       //   headers.setLocation(new URI(poDTO.getId().getHref()));
//          return new ResponseEntity<List<PlantInventoryEntryDTO>>(availablePlants, headers, HttpStatus.CREATED); 
//       
//    }
    
    @RequestMapping(method=GET, path="/{po_id}")
    @Secured({"ROLE_SiteEngineerr","ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
    public ResponseEntity<PurchaseOrderDTO> GetPOStatus(@PathVariable("po_id") Long po_id  ){
    	
    	
    	PurchaseOrderDTO PODTO=salesService.findPurchaseOrder(PurchaseOrderID.of(po_id));
    	HttpHeaders header=new HttpHeaders();
    	return new ResponseEntity<PurchaseOrderDTO>(PODTO,header,HttpStatus.CREATED); 
    	
    }
    
   
	 @RequestMapping(method=PUT, path="/{_id}/reject")    //PS8        _id is the id of PO
	 @Secured({"ROLE_SiteEngineerr","ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
	 public ResponseEntity<PurchaseOrderDTO> POCancelation(@PathVariable("_id") String id) throws Exception {
       	    	
		   	Long Id=	Long.parseLong(id);
		   	System.out.println("Post POCancelation/id   "+ id);
		   	salesService.checkCancelationPosibility(Id);   
		    salesService.RejectPurchaseOrder(PurchaseOrderID.of(Id));
		    PurchaseOrderDTO po = salesService.findPurchaseOrder(PurchaseOrderID.of(Id));
		    
		    HttpHeaders header=new HttpHeaders();
		    return new ResponseEntity<PurchaseOrderDTO>(po,header,HttpStatus.CREATED);     
		    
		    
		    
      }
    
    @RequestMapping(method = POST, path = "")     //PS4 /{cu_id}
    @Secured({"ROLE_SiteEngineerr","ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
    public ResponseEntity<PurchaseOrderDTO>  createPO(@RequestBody PurchaseOrderDTO poDTO/*,@PathVariable( "cu_id") Long cu_id*/) 
    	throws Exception {
    	
    	System.out.println("Created new PO:  ");
        poDTO = salesService.createPurchaseOrder(poDTO/*,cu_id*/);

        HttpHeaders headers = new HttpHeaders();
        try{
        headers.setLocation(new URI(poDTO.getId().getHref()));
        return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED); 
        }
        catch(Exception e){
        	System.out.println("Failed to make plant");
        	return null;
        }
    }
    
    @RequestMapping(method=PUT, path="")// /PS7
    @Secured({"ROLE_SiteEngineerr","ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
    public ResponseEntity<PurchaseOrderDTO> EditPO(@RequestBody PurchaseOrderDTO Editedpo){
    	PurchaseOrderDTO po= salesService.EditePO(Editedpo/*,cu_id*/);
    	HttpHeaders header=new HttpHeaders();
    	return new ResponseEntity<PurchaseOrderDTO>(po,header, HttpStatus.CREATED);
    }
    
    
    
    
//
//    @RequestMapping(method = DELETE, path = "/{oid}/accept")
//    @Secured({"ROLE_SiteEngineerr","ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
//    public ResponseEntity<PurchaseOrderDTO> RejectingPO( @PathVariable( "oid") Long id) throws Exception {
//         
//    //	InventorySpecifications.findPurchaseOrder(PurchaseOrderID.of(id));
//    	
//    	salesService.RejectPurchaseOrder(PurchaseOrderID.of(id));     
//         
//         PurchaseOrderDTO poDTO = salesService.getPurchaseOrder(PurchaseOrderID.of(id));
//         HttpHeaders headers = new HttpHeaders();
//         headers.setLocation(new URI(poDTO.getId().getHref()));
//         return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED);
//         
//    }
    
//    @RequestMapping(method = PUT, path = "/orders/{oid}")
//    @Secured({"ROLE_SiteEngineerr","ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
//    public ResponseEntity<PurchaseOrderDTO> reSubmitPO( @PathVariable("oid") Long id) throws Exception {
//         salesService.PendingPurchaseOrder(PurchaseOrderID.of(id));       
//    
//         PurchaseOrderDTO poDTO = salesService.getPurchaseOrder(PurchaseOrderID.of(id));
//         HttpHeaders headers = new HttpHeaders();
//         headers.setLocation(new URI(poDTO.getId().getHref()));
//         return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED);
//    
//    }
    
//    @RequestMapping(method = POST, path = "/orders/{oid}/accept")
//    @Secured({"ROLE_SiteEngineerr","ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
//    public ResponseEntity<PurchaseOrderDTO> acceptPO( @PathVariable("oid") Long id) throws Exception {
//         salesService.PendingPurchaseOrder(PurchaseOrderID.of(id));       
//   
//         PurchaseOrderDTO poDTO = salesService.getPurchaseOrder(PurchaseOrderID.of(id));
//         HttpHeaders headers = new HttpHeaders();
//         headers.setLocation(new URI(poDTO.getId().getHref()));
//         return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED);
//    }
//    
//    @RequestMapping(method = DELETE, path = "/orders/{oid}")
//    @Secured({"ROLE_SiteEngineerr","ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
//    public ResponseEntity<PurchaseOrderDTO> ClosePO( @PathVariable( "oid") Long id) throws Exception {
//         salesService.ClosePurchaseOrder(PurchaseOrderID.of(id));       
//    
//         PurchaseOrderDTO poDTO = salesService.getPurchaseOrder(PurchaseOrderID.of(id));
//         HttpHeaders headers = new HttpHeaders();
//         headers.setLocation(new URI(poDTO.getId().getHref()));
//         return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED);
//    
//    }
//    
//    @RequestMapping(method = POST, path = "/orders/{oid}/extention")
//    @Secured({"ROLE_SiteEngineerr","ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
//    public ResponseEntity<PurchaseOrderDTO> extendRentalPeriodPO(@PathVariable( "oid") Long id) throws Exception {
//         salesService.ClosePurchaseOrder(PurchaseOrderID.of(id));    
//         
//         PurchaseOrderDTO poDTO = salesService.getPurchaseOrder(PurchaseOrderID.of(id));
//         HttpHeaders headers = new HttpHeaders();
//         headers.setLocation(new URI(poDTO.getId().getHref()));
//         return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED);
//              
//    }
//    
  /*
    
    @RequestMapping(method = DELETE, path = "orders/{oid}/extentions/{eid}/accept")
    public PurchaseOrderDTO rejectRPExtention(@PathVariable Long oid,@PathVariable Long eid ) throws Exception {
        PurchaseOrderDTO poDTO = salesService.rejectRPExtension(PurchaseOrderID.of(oid), eid);
        return poDTO;
    }
    //*/
    
    
    
    
    
    
//    @RequestMapping(method = POST, path = "/{id}")
//    @Secured({"ROLE_SiteEngineerr","ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
//    public PurchaseOrderDTO showPurchaseOrder(@PathVariable Long id) throws Exception {
//        PurchaseOrderDTO poDTO = salesService.findPurchaseOrder(PurchaseOrderID.of(id));
//        return poDTO;
//    }
//    
    
    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    public String bindExceptionHandler(Exception ex) {
        return ex.getMessage();
    }
    
}
