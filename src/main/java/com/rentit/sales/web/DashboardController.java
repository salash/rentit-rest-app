package com.rentit.sales.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentit.customer.dto.CustomerDTO;
import com.rentit.customer.dto.UserDTO;
import com.rentit.customer.repository.AuthoritiesRepository;
import com.rentit.customer.repository.UserRepository;
import com.rentit.customer.service.UserService;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.service.InventoryService;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryEntryID;
import com.rentit.invoice.SendRecieveInvoice;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.application.service.SalesService;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.model.PurchaseOrderID;
import com.rentit.sales.domain.repository.PurchaseOrderRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.security.Principal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {
    @Autowired
    InventoryService inventoryService;
    @Autowired
    SalesService salesService;

    @Autowired
    UserService userService;
    
    @Autowired
    SendRecieveInvoice SendRecieveInvoice;
    
    @Autowired
    UserRepository userRepo;
    
    @Autowired
    AuthoritiesRepository authorizationRepo;
    
    
    @RequestMapping(method=GET, path="/registration")  
    public String Registeration(Model model){
        model.addAttribute("newCustomer", new UserDTO());
        System.out.println("Registeration page loaded. ");
    	return "fragments/registeration";
    }
 ///*  
    @RequestMapping(method=POST, path="/NewUserRegisteration")  
    public String RegisterNewCustomer(/* Model model,*/@ModelAttribute UserDTO userDTO ){
    	System.out.println("Name:"+ userDTO.getName()
    	                   +"  Phone:"+userDTO.getPhone()
    	                   +"  Address:  "+userDTO.getAddress()
    	                   +"  Username:  "+userDTO.getUserName()
    	                   +"  Password:  "+userDTO.getPassword());
    	
    	userService.saveUser(userDTO);
		System.out.println("All Users are: ");
		System.out.println(userRepo.findAll());
		
		System.out.println("All Authorizations are: ");
		System.out.println(authorizationRepo.findAll());
		
    	
    	return  "redirect:"+ "/dashboard/ShowRequestedPlantsInPO";//"fragments/registeration";f
    }
 //  */
    
    @RequestMapping(method=GET, path="/ShowRequestedPlantsInPO")    //PS5
    @Secured({"ROLE_Manager"})
    public String ShowRequestedPlantsInPO(Model model,HttpSession session){
    	
    	ArrayList<PurchaseOrderDTO> POs = salesService.findByStatus(POStatus.PENDING);
    	model.addAttribute("POList",POs );
    	
    	return "dashboard/orders/show-POs";
    }
    
    @RequestMapping(method = GET, path = "/invoice")
    @Secured({"ROLE_Manager"})
    public String sendInvoice(Model model) throws Exception {
    	try{
    	ArrayList<PurchaseOrder> purchaseOrder = salesService.findExpiredPOs(LocalDate.now());
        System.out.println("Status is:  "+purchaseOrder.get(0).getStatus());
        model.addAttribute("POList", purchaseOrder);
        return "dashboard/orders/Invoice" ;
    	}
    	catch(Exception e){
    		return "dashboard/orders/Invoice";
    	}
    	}
    
    
    @RequestMapping(method=POST, path="/makeAndSendInvoice/{email:.+}")
    @Secured({"ROLE_Manager"})
    public String makeAndSendInvoice(@PathVariable("email") String email) throws Exception {
    	
    	System.out.println("Invoice is sent to..................."+email);
    	SendRecieveInvoice.sendInvoice(email);
    	return "dashboard/orders/Invoice";
    }  
    
   	 @RequestMapping(method=POST, path="ShowPlant/{_id}")
   	 @Secured({"ROLE_Manager"})
      public String ShowPlant(Model model,@PathVariable("_id") Long id) throws Exception {
        	    	
       	    PurchaseOrderDTO po = salesService.getPurchaseOrder(PurchaseOrderID.of(id));
       	    
       	    PlantInventoryEntryDTO p = po.getPlant();
       	    p.setPoID(id);
       	    System.out.println("PO ID is ..................."+p.getPoID());
       	    
       	    model.addAttribute("plant",p);
       	  
         	return "dashboard/orders/show-plant";
       }
 
 	 @RequestMapping(method=PUT, path="/deliverplant")
 	 @Secured({"ROLE_Manager"})
 	 public String DeliverPlant(Model model,@RequestParam Long _id,@RequestParam Long poID ) throws Exception {       	    	
   	
   	     System.out.println("Plant should be delivered");
   	      inventoryService.changePlantStatusToBeDelivered(_id,poID);
   	          	 
   	  // httpServletResponse.setHeader("Location", projectUrl);
        	return "redirect:"+ "/dashboard/ShowRequestedPlantsInPO";
      }
//////////////////////////   	 
   	 @RequestMapping(method=DELETE, path="reject")
   	 @Secured({"ROLE_Manager"})
   	 public String RejectPO(Model model,@RequestParam Long _id,@RequestParam Long poID) throws Exception {
       	    	
   //	Long Id=	Long.parseLong(_id);
   	System.out.println("Purchase Order ID is Rejected:   "+ _id);
   	
     salesService.RejectPurchaseOrder(PurchaseOrderID.of(poID));
  // 	boolean b = inventoryService.changePlantStatusToBeDelivered(Id);
      	          	 
        	return "redirect:"+ "/dashboard/ShowRequestedPlantsInPO";
      }
////////////////////////////////////////////////////////////
   	 
	 @RequestMapping(method=GET, path="depo/dispatch")
	 @Secured({"ROLE_Manager","ROLE_WAREHOUSE_KEEPER"})
	 public String DepoManager(Model model) throws Exception {
       	    	
	   	 System.out.println("Depo/ReadyToDispatch:   ");
	   	 List<PlantInventoryEntryDTO> plantList=inventoryService.findAllPlantInventoryEntriesWhichVerifiedToDeliver();
	   
	     model.addAttribute("plantList", plantList); 
         return "dashboard/depo/ready-to-dispatch";
      }
 //////////////////////////////////////////////////7  	 
     @RequestMapping(method=PUT, path="deliver/{_id}")
     @Secured({"ROLE_Manager","ROLE_DELIVERER"})
      public String dispatched(Model model,@PathVariable("_id") String id) throws Exception {
        	    	
    	Long Id=	Long.parseLong(id);
    	System.out.println("Plant ID to dispatch is:   "+ id);
    	
    	//inventoryService.changePlantStatusToDispatched(Id);          	 
    	inventoryService.changePlantStatusToDelivered(Id);
    	return "redirect:/dashboard/depo/delivery";
       }
//////////////////////////////////////////////////   delivered
     @RequestMapping(method=PUT, path="dispatch/{dispatch_id}")
     @Secured({"ROLE_Manager","ROLE_WAREHOUSE_KEEPER"})
     public String delivered(Model model,@PathVariable("dispatch_id") String id) throws Exception {
       	    	
    	Long Id=	Long.parseLong(id);
   		System.out.println("Plant ID to dispatch is:   "+ id);
  // 		inventoryService.changePlantStatusToDelivered(Id);   
   		inventoryService.changePlantStatusToDispatched(Id);
   		return "redirect:/dashboard/depo/dispatch";
      }
///////////////////////////   	
     @RequestMapping(method=PUT, path="returnedbycustomer/{_id}")
     @Secured({"ROLE_Manager","ROLE_DELIVERER"})
     public String ReturnedByCustomer(Model model,@PathVariable("_id") String id) throws Exception {
       	    	
   	Long Id=	Long.parseLong(id);
   	System.out.println("Here is Returned to depo:   "+ id); 
	 inventoryService.changePlantStatusToReturned(Id);
   	return "redirect:/dashboard/depo/delivery";
      }  
////////////////////////////////////Returned
     @RequestMapping(method=PUT, path="returned/{_id}/{po_id}")             //PS11
     @Secured({"ROLE_Manager","ROLE_WAREHOUSE_KEEPER"})
     public String Returned(Model model/*,@RequestParam Long _id,@RequestParam Long poID*/
    		 ,@PathVariable("_id") String _id,@PathVariable("po_id") String poID) throws Exception {
       	    	
     	Long plantId=	Long.parseLong(_id);
     	System.out.println("Returened plant is:   "+ _id+"   poid is:    "+poID);
     	PlantInventoryEntry p = inventoryService.findPlantByID(PlantInventoryEntryID.of(plantId));
   	    
     ///*
     	Long POId=	Long.parseLong(poID);
       	PurchaseOrderID po_id = PurchaseOrderID.of(POId); 
     	salesService.ChangStatusOfPurchaseOrder(po_id);   
     	inventoryService.changePlantStatusToReturnedToDepo(plantId,POId);  // 
     	//*/
       	
     	
     	return "redirect:/dashboard/catalog/form";
      
     } 
     
//////////////////////////////////////    
    
     @RequestMapping(method=GET, path="/depo/delivery")
     @Secured({"ROLE_Manager","ROLE_DELIVERER"})
     public String DepoManager_Delivered(Model model) throws Exception {
    
           List<PlantInventoryEntryDTO> plantList=inventoryService.findAllDispatchedPlantInventoryEntries();
      	   	model.addAttribute("plantList", plantList); 
        	return "dashboard/depo/delivered-or-rejected";
      }
////////////////////////////////////////////
     @RequestMapping(method=GET, path="depo/plantsreturned")
     @Secured({"ROLE_Manager","ROLE_WAREHOUSE_KEEPER"})
     public String DepoManager_DeliveredPlants(Model model) throws Exception {
       	    	
   	 System.out.println("depo/plantsreturned:   ");
   	List<PlantInventoryEntryDTO> plants= salesService.findDeliveredPlants();
   //	 List<PlantInventoryEntryDTO> plantList=inventoryService.findAllDeliveredPlantInventoryEntries();
 
      	   	model.addAttribute("plantList", plants); 
        	return "dashboard/depo/plants-returned";
      } 
///////////////////////////////////    
    @RequestMapping(method = GET, path = "/catalog/form")
    @Secured({"ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
    public String getQueryForm(Model model) {
        model.addAttribute("catalogQuery", new CatalogQueryDTO());
        return "dashboard/catalog/query-form";
    }
   	 
   	
    @RequestMapping(method = POST, path = "/catalog/query")
    @Secured({"ROLE_Manager","ROLE_WAREHOUSE_KEEPER","ROLE_DELIVERER"})
    public String queryPlantCatalog(Model model,@RequestBody CatalogQueryDTO query) {
    	
    	List<PlantInventoryEntryDTO> availablePlants = inventoryService.findAvailablePlants(
                query.getName(),
                query.getRentalPeriod().getStartDate(),
                query.getRentalPeriod().getEndDate());
    	
        model.addAttribute("plants",availablePlants );
        PurchaseOrderDTO po = new PurchaseOrderDTO();
        po.setRentalPeriod(query.getRentalPeriod());
        model.addAttribute("po", po);
        return "dashboard/catalog/query-result";
    }
}
