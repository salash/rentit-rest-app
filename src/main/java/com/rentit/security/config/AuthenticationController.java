package com.rentit.security.config;

import java.util.LinkedList;
import java.util.List;

import javax.management.relation.Role;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;

@RequestMapping("/api/authenticate")
@Controller
public class AuthenticationController {

    @RequestMapping(method=POST)
   // @CrossOrigin
// /*   
    public List<String> authenticate() throws JsonProcessingException {
    /*	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	String username = authentication.getName();
    	System.out.println("Authority of username is: "+username);
    	*/
    	
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<String> roles = new LinkedList<String>();

        if (principal instanceof UserDetails) {
            UserDetails details = (UserDetails) principal;
            for (GrantedAuthority authority: details.getAuthorities()){
            	System.out.println("Authority is: "+authority.getAuthority());
                roles.add(authority.getAuthority());
            }
        }
        System.out.println("Login was tried");//+roles.get(0) );  
        return roles;
    }

    @ExceptionHandler(value={SecurityException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public void handleSecurityException() {
    } //*/
}