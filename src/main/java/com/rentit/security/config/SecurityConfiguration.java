package com.rentit.security.config;


import javax.sql.DataSource;
///*
//import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
///*
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
//*/
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.spring4.SpringTemplateEngine;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration {


  @Configuration
  static class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter { 
	      @Autowired
    private DataSource dataSource;

    public void init(AuthenticationManagerBuilder auth) throws Exception {
      auth.jdbcAuthentication()
            .dataSource(dataSource)
            .usersByUsernameQuery(
                    "select username,password,enabled from user where username=?")
            .authoritiesByUsernameQuery(
                    "select username,authority from authorities where username=?");
    }
  
  }

  @Configuration
  static class ClientWebConfig extends WebMvcConfigurerAdapter { 
    public void addViewControllers(ViewControllerRegistry registry) {
      registry.addViewController("/login").setViewName("fragments/login");
    }
  }
  

  @Configuration
  static class WebFormsSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter { 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
   
    	      http.authorizeRequests()
    	 
    	      .antMatchers("/dashboard/registration").permitAll()
    	      .antMatchers("/dashboard/NewUserRegisteration").permitAll()
    	      
            .antMatchers("/dashboard/**").hasRole("Manager")
            .antMatchers("/dashboard/**").authenticated()
            
            .antMatchers("/dashboard/**").hasRole("WAREHOUSE_KEEPER")
            .antMatchers("/dashboard/**").authenticated()
            
            .antMatchers("/dashboard/**").hasRole("DELIVERER")
            .antMatchers("/dashboard/**").authenticated()
            
            .and()
            
            .formLogin().loginPage("/login").permitAll()
            .defaultSuccessUrl("/dashboard/catalog/form")
            .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"));    
    	      
    }
  }
}

