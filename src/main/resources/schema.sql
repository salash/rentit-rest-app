/*
create table if not exists users (username varchar(50) not null primary key, password varchar(50) not null, enabled boolean not null, name varchar(50), address varchar(50), phone varchar(50));
create table if not exists authorities (username varchar(50) not null, authority varchar(50) not null, constraint FK_AUTHORITIES_USERS foreign key(username) references users(username));

insert into users (username, password, enabled) SELECT 'admin', 'admin', true where not exists (select username, password, enabled from users where username = 'admin' and password = 'admin');
insert into authorities (username, authority)   SELECT 'admin', 'ROLE_ADMIN' where not exists (select username, authority from authorities where username = 'admin' and authority = 'ROLE_ADMIN');

insert into users (username, password, enabled) SELECT 'warehouse_keeper', 'warehouse_keeper', true where not exists (select username, password, enabled from users where username = 'warehouse_keeper' and password = 'warehouse_keeper');
insert into authorities (username, authority)   SELECT 'warehouse_keeper', 'ROLE_WAREHOUSE_KEEPER' where not exists (select username, authority from authorities where username = 'warehouse_keeper' and authority = 'ROLE_WAREHOUSE_KEEPER');

insert into users (username, password, enabled) SELECT 'deliverer', 'deliverer', true where not exists (select username, password, enabled from users where username = 'deliverer' and password = 'deliverer');
insert into authorities (username, authority)   SELECT 'deliverer', 'ROLE_DELIVERER' where not exists (select username, authority from authorities where username = 'deliverer' and authority = 'ROLE_DELIVERER');


insert into authorities (username, authority)   SELECT 'username', 'ROLE_CUSTOMER' where not exists (select username, authority from authorities where username = 'deliverer' and authority = 'ROLE_CUSTOMER');
*/




CREATE SEQUENCE IF NOT EXISTS PlantInventoryEntryIDSequence START WITH 100 INCREMENT BY 1;
CREATE SEQUENCE IF NOT EXISTS PlantInventoryItemIDSequence START WITH 100 INCREMENT BY 1;
CREATE SEQUENCE IF NOT EXISTS PlantReservationIDSequence START WITH 100 INCREMENT BY 1;
CREATE SEQUENCE IF NOT EXISTS MaintenancePlanIDSequence START WITH 100 INCREMENT BY 1;
CREATE SEQUENCE IF NOT EXISTS MaintenanceTaskIDSequence START WITH 100 INCREMENT BY 1;
CREATE SEQUENCE IF NOT EXISTS PurchaseOrderIDSequence START WITH 100 INCREMENT BY 1;

CREATE SEQUENCE IF NOT EXISTS CustomerIDSequence START WITH 100 INCREMENT BY 1;
CREATE SEQUENCE IF NOT EXISTS CancelationIDSequence START WITH 100 INCREMENT BY 1;

CREATE SEQUENCE IF NOT EXISTS UserIDSequence START WITH 100 INCREMENT BY 1;
CREATE SEQUENCE IF NOT EXISTS AuthorityIDSequence START WITH 100 INCREMENT BY 1;



